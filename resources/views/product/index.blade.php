@extends('layouts.app')

@section('title', 'Produktai')

@section('content')
    <h1>Kategorija: {{ $product->name }}</h1>

    @if (count($product->categories) > 0)
    <h2>Kategorijai priskirti produktai</h2>
    <ul>
        @foreach ($product->categories as $category)
        <li>{{ $category->name }}</li>
        @endforeach
    </ul>
    @endif
@endsection
