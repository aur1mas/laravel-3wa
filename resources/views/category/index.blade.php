@extends('layouts.app')

@section('title', 'Kategorijos')

@section('content')
    <h1>Kategorija: {{ $category->name }}</h1>

    @if (count($category->products) > 0)
    <h2>Kategorijai priskirti produktai</h2>
    <ul>
        @foreach ($category->products as $product)
        <li>{{ $product->name }}</li>
        @endforeach
    </ul>
    @endif
@endsection
